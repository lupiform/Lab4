import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!

    public var people: [People] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        downloadData()

        self.tableView.reloadData()

        tableView.delegate = self
        tableView.dataSource = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return people.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = people[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? PeopleViewController {
            destination.person = people[(tableView.indexPathForSelectedRow?.row)!]
        }
    }

    func downloadData() {
        let url = URL(string: "https://swapi.co/api/people")
        URLSession.shared.dataTask(with: url!) { (data, response, error) in

            if error == nil {
                
                if let responseData = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: responseData,
                                                                    options: JSONSerialization.ReadingOptions.allowFragments) as! [String: Any]

                        DispatchQueue.main.async {

                            if let peoples = json ["results"] as? NSArray{
                        
                                for diction in peoples {
                            
                                    let data = diction as! NSDictionary
                                    let parsedData = People()
                                    parsedData.name = data["name"] as! String
                                    parsedData.hair_color = data["hair_color"] as! String
                                    parsedData.eye_color = data["eye_color"] as! String
                                    parsedData.birth_year = data["birth_year"] as! String
                                    parsedData.gender = data["gender"] as! String

                                    self.people.append(parsedData)
                                }
                            }
                    self.tableView.reloadData()

                        }
                    }
                    catch {
                        print("Could not serialize into json")
                    }
                }
            }
            
            else {
                print("error has occurred")
            }
        }.resume()
    }
}
