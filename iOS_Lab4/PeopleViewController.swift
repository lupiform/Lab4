import UIKit

class PeopleViewController: UIViewController {

    var person:People?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var hairColorLabel: UILabel!
    @IBOutlet weak var eyeColorLabel: UILabel!
    @IBOutlet weak var birthYearLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        nameLabel.text = person?.name
        hairColorLabel.text = person?.hair_color
        eyeColorLabel.text = person?.eye_color
        birthYearLabel.text = person?.birth_year
        genderLabel.text = person?.gender

    }
    
}
